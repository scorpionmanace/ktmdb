# K - The Movie Db

## Steps to follow

```
- Clone / Download unzip the project
- Yarn install
- Yarn serve
- open localhost:8080 (or 81 as the case may be)

-- Notes:
- Search functionality should wokd as you type in the search.
- Current functionality doesn't support pagination, @todo item to be addressed in future

```

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
