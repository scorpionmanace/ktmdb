import Vue from 'vue';
import VueRouter from 'vue-router';
import dashboard from '@/views/dashboard.view.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: dashboard,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
