'use strict';

import { MediaType } from '../models/mediatype.enums';

export default {
  // data: {
  //   request: {
  //     current: null,
  //     dashboardTitle: null,
  //   },
  // },
  // created() {
  //   this.$eventHub.$on('request', (fn, title) => {
  //     this.request.current = fn;
  //     this.dashboardTitle = title;
  //   });
  // },
  methods: {
    ucfirst(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    },
    poster(img) {
      return img ? `https://image.tmdb.org/t/p/w300${img}` : '';
    },

    backdrop(img) {
      return img ? `https://image.tmdb.org/t/p/original${img}` : '';
    },

    castImg(img) {
      return img ? `https://image.tmdb.org/t/p/w200${img}` : '';
    },
    missingImage() {
      return '/missing.png';
    },
    isVideoType(media) {
      switch (media.media_type) {
        case MediaType.movie:
        case MediaType.tv:
          return true;
        default:
          return false;
      }
    },
  },
};
