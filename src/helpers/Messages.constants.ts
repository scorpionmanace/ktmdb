export default class MessageConstants {
  static upcoming: string = 'Upcoming Movies';

  static latest: string = 'Latest Movies';

  static popular: string = 'Popular Movies';

  static search: string = 'Search';

  static searchMovies: string = 'Search Movies';

  static searchedResults: string = 'Searched Results';

  static thisYearReleases: string = 'This year releases';

  static nowPlayingInTheater: string = 'Now Playing in Theater';

  static commingSoon: string = 'Comming Soon';

  static kids: string = 'Kids Movies';
}
