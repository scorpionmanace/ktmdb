import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
  },
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#8E24AA',
        accent: '#82B1FF',
        secondary: '#E1BEE7',
      },
      light: {
        primary: '#3f51b5',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        white: '#FFFFFF',
      },
    },
  },
});
