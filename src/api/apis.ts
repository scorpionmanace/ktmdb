/* eslint-disable indent */
import moment from 'moment';

export default class APIS {
  public static multiSearch = (
    query: string,
    adultAllowed: boolean = false,
    page: number = 1,
  ): string => `${process.env.VUE_APP_DB_URI}/search/multi?api_key=${process.env.VUE_APP_API_KEY}&language=en-US&page=${page}&include_adult=${adultAllowed}&query=${query}`;

  public static popularMovies = (page: number = 1): string => `${process.env.VUE_APP_DB_URI}/discover/movie?api_key=${process.env.VUE_APP_API_KEY}&page=${page}&sort_by=popularity.desc`;

  public static nowPlaying = (page: number = 1): string => `${process.env.VUE_APP_DB_URI}/discover/movie?api_key=${
      process.env.VUE_APP_API_KEY
    }&page=${page}&primary_release_date.gte=2019-10-01&primary_release_date.lte=${moment().format(
      'YYYY-MM-DD',
    )}`;

  public static recentMovies = (page: number = 1): string => `${process.env.VUE_APP_DB_URI}/discover/movie?api_key=${
      process.env.VUE_APP_API_KEY
    }&page=${page}&primary_release_date.gte=2019-01-01&primary_release_date.lte=${moment().format(
      'YYYY-MM-DD',
    )}`;
}
