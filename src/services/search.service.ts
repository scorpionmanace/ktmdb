import axios from 'axios';
import ResponseModel from '../models/response.model';
import APIS from '../api/apis';

export default class SearchService {
  public static getMultiSearch = async (query: string, adultAllowed: boolean = false) => {
    const response = new ResponseModel();
    try {
      const listData: any = await axios.get(APIS.multiSearch(query, adultAllowed));
      response.setData(listData.data);
    } catch (APIException) {
      response.setErr(APIException);
    }
    return response;
  };
}
