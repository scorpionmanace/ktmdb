'use strict';

import axios from 'axios';
import ResponseModel from '../models/response.model';
import APIS from '../api/apis';

export default class DiscoverService extends ResponseModel {
  /**
   * Fetches most popular movie from the movie database
   */
  public static getPopularMovies = async (page = 1) => {
    const result = new ResponseModel();
    try {
      const listData = await axios.get(APIS.popularMovies(page));
      result.setData(listData.data);
    } catch (APIException) {
      result.setErr(APIException);
    }
    return result;
  };

  /**
   * Fetches now playing movies
   */
  public static getNowPlaying = async (page = 1) => {
    const result = new ResponseModel();
    try {
      const listData = await axios.get(APIS.nowPlaying(page));
      result.setData(listData.data);
    } catch (APIException) {
      result.setErr(APIException);
    }
    return result;
  };

  /**
   * Fetches most recent movies from the movie database
   */
  public static getRecentMovies = async (page = 1) => {
    const result = new ResponseModel();
    try {
      const listData = await axios.get(APIS.recentMovies(page));
      result.setData(listData.data);
    } catch (APIException) {
      result.setErr(APIException);
    }
    return result;
  };
}
