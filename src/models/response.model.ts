export default class ResponseModel {
  private data: any;

  private err: any;

  getData = () => this.data;

  setData = (data: any) => {
    this.data = data;
  };

  getErr = () => this.err;

  setErr = (err: any) => {
    this.err = err;
  };
}
