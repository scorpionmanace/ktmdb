export enum MediaType {
  person = 'person',
  movie = 'movie',
  tv = 'tv'
}

export default { MediaType };
