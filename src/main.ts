import Vue from 'vue';
import './plugins/axios';
import lodash from 'lodash';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

// Global event processing
Vue.prototype.$eventHub = new Vue();
Vue.prototype._ = lodash;
new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app');
